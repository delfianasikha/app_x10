package delfia.puja.app_x10

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*


class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegis.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var email = edRegUsername.text.toString()
        var password = edRegPassword.text.toString()
        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(
                this,
                "Username and password can't be empty ", Toast.LENGTH_SHORT
            ).show()
        } else {
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("Registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    progressDialog.hide()
                    if (!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "Succesfully Register", Toast.LENGTH_SHORT).show()
                    finish()
                }
                .addOnFailureListener {
                    progressDialog.hide()
                    Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_SHORT).show()
                }
        }

    }
}